#!/bin/bash

if [ -z $1 ]
then
    echo "please provide a migration name in parameter"
    exit 0
fi

cp template-migration.xml src/main/resources/liquibase/$(date -d "today" +"%Y%m%d%H%M%S")-$1.xml
