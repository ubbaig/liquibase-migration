### Liquibase Migration Project

Generate Migration file using generate-migration.bat|.sh file according to your platform, like:

generate-migration.sh <provide-name-here>

the above command will generate a  migration in your resource/liquibase directory